import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';

function ShoeColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const shoe = data.shoe;
                return (
                    <div key={shoe.id} className="card mb-3 shadow">
                        <img
                            src={shoe.picture_url} className="card-img-top" alt='Picture of Shoe' />
                        <div className="card-body">
                            <h5 className="card-title">{shoe.manufacturer}, {shoe.model_name} </h5>
                            <h6 className="card-subtitle mb-2 text-muted">{shoe.color}</h6>
                            <p className="card-text">{shoe.bin.closet_name}
                            </p>
                            <div className="card-footer">
                                <button type="button" className="btn btn-danger" onClick={() => props.onDelete(shoe.id)}>Delete</button>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}


function ShoesList() {
    const [shoeColumns, setShoeColumns] = useState([[], [], []]);

    async function getShoes() {
        const url = "http://localhost:8080/api/shoes/";

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let shoe of data.shoe) {
                    const shoeUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
                    requests.push(fetch(shoeUrl));
                }
                const responses = await Promise.all(requests);
                const shoeColumns = [[], [], []];

                let i = 0;
                for (let shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        shoeColumns[i].push(details)
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    }else {
                        console.error(shoeResponse);
                    }
                }
                setShoeColumns(shoeColumns);
            }
        } catch (e) {
            console.error(e);
        }
    };
    const handleDelete = async (shoeId) => {
        const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
            method: "DELETE",
        });
        if (response.ok) {
            getShoes();
        }
    };
    useEffect(() => {
        getShoes();
    }, []);


    return (
        <div className="container">
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">Shoes</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4 text-center">
                        Shoes, Shoes, Shoes, Who doesn't Love them!
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <NavLink to="/shoes/create" className="btn btn-primary btn-lg px-4 gap-3">Click to add a Shoe</NavLink>
                    </div>
            </div>
        </div>

            <div className="row">
                {shoeColumns.map((shoesList, index) => {
                    return (
                        <ShoeColumn key={index} list={shoesList} onDelete={handleDelete} />
                    );
                })}
            </div>
        </div>
    )
}

export default ShoesList;









// import React, { useState, useEffect } from 'react';
// import { NavLink } from 'react-router-dom';


// function ShoesList() {
//     const [shoes, setShoes] = useState([])

//     const getData = async () => {
//         const response = await fetch("http://localhost:8080/api/shoes/");

//         if (response.ok) {
//             const data = await response.json();
//             setShoes(data.shoes)
//         }
//     }

//     useEffect(()=>{
//         getData()
//     }, [])

//     return (
//         <table className="table table-striped">
//             <thead>
//                 <tr>
//                     <th>Name</th>
//                     <th>Bin</th>
//                 </tr>
//             </thead>
//             <tbody>
//                 {shoes.map(shoe => {
//                     return (
//                         <tr key={shoe.href}>
//                             <td>{ shoe.name }</td>
//                             <td>{ shoe.bin }</td>
//                         </tr>
//                     );
//                 })}
//             </tbody>
//         </table>
//     );
// }

// export default ShoesList;
