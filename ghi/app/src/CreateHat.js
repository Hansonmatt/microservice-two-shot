import React, { useState, useEffect } from 'react';

function CreateHatForm() {
    const [locations, setLocations] = useState([]);
    const [fabricType, setFabricType] = useState('')
    const [styleName, setStyleName] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [location, setLocation] = useState('')

    const getData = async () => {
        const url = 'http://localhost:8100/api/locations'
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric_type = fabricType;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;


        const url = `http://localhost:8090/api/hats/`


        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFabricType('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    }

    const handleFabricChange = (e) => {
        const value = e.target.value;
        setFabricType(value);
    }
    const handleStyleChange = (e) => {
        const value = e.target.value;
        setStyleName(value);
    }
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }
    const handleUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    }




    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} value={fabricType} placeholder="Fabric Type" required type="text" name="fabric_type" id="fabric_type" className="form-control" />
                            <label htmlFor="fabric_type">Fabric Type</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} value={styleName} placeholder="Hat Style" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="style_name">Hat Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleUrlChange} value={pictureUrl} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                <option value="">Choose a closet</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                                    )
                                })};
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CreateHatForm;
