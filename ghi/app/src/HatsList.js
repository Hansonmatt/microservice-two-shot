import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';

function HatColumns(props) {

    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.id} className="card mb-3 shadow">
                        <img
                            src={hat.picture_url} className="card-img-top" alt='Hat Pics' />
                        <div className="card-body">
                            <h5 className="card-title">{hat.fabric_type}, {hat.style_name} </h5>
                            <h6 className="card-subtitle mb-2 text-muted">{hat.color}</h6>
                            <p className="card-text">{hat.location.closet_name}
                            </p>
                            <div className="text-center">
                                <button type="button" className="btn btn-danger" onClick={() => props.onDelete(hat.id)}>Delete</button>
                            </div>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}


function HatList() {


    const [columns, setColumns] = useState([[], [], []]);
    async function getHats() {
        const url = "http://localhost:8090/api/hats/";
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hat) {
                    const hatUrl = `http://localhost:8090/api/hats/${hat.id}`;
                    requests.push(fetch(hatUrl));
                }
                const responses = await Promise.all(requests);
                const columns = [[], [], []];

                let i = 0;
                for (let hatResponse of responses) {
                    if (hatResponse.ok) {
                        const details = await hatResponse.json();
                        columns[i].push(details)
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                        }else {
                            console.error("this????", hatResponse);
                    }
                }
                setColumns(columns);
            }
        } catch (error) {
            console.error(error);
        }
    };
    const handleDelete = async (hatId) => {
        const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
            method: "DELETE",
        });
        if (response.ok) {
            getHats();
        }
    };
    useEffect(() => {
        getHats();
    }, []);


    return (
        <div className="container">
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">My hats</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4 text-center">
                        All of your hats in one place!
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <NavLink to="/hats/create" className="btn btn-primary btn-lg px-4 gap-3">Got a new hat?</NavLink>
                    </div>
            </div>
        </div>
            <div className="row">
                {columns.map((hatList, index) => {
                    return (
                        <HatColumns key={index} list={hatList} onDelete={handleDelete} />
                    );
                })}
            </div>
        </div>
    )
}

export default HatList;
