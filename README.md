# Wardrobify

Team:

* Malek Byam - Shoes
* Matthew Hanson - Hats

## Design

## Shoes microservice

I messed with some files in node_modules I probably shouldn't have and broke react and couldn't figure out how to get it working again until I ran out of time.

## Hats microservice

My models properties follow the guidelines given in the learn content. To be able to access a location for the Hats model, I created LocationVO and assigned it to the Hats model using a foreign key. I used all fields from the Location model in the wardrobe app in the case that I decide to use them to further break down the location for hats on my webpage.

A poller was needed for LocationVo to actually be passed the locationn data that it needed. I used the update or create to either update or create an import_href. This allowed me to be able to specifiy and select a location in my create hat form and be able to assocaiate a hat with a location.
