from django.urls import path
from .views import show_shoe, list_shoes


urlpatterns = [
    path("shoes/", list_shoes , name="list_shoes"),
    path("bins/<int:bin_vo_id>/shoes/", list_shoes, name="list_shoes"),
    path("shoes/<int:id>/", show_shoe, name="show_shoe"),
]
