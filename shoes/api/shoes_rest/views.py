from django.shortcuts import render


from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

# move into above to combine delete get put
@require_http_methods(["DELETE", "GET"])
def shoe_delete(request, pk):
    if request.method == "DELETE":
        try:
            count, _ = Shoe.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Invalid shoe id"})
    else:
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )



def show_shoe(request, id):
    shoe = Shoe.objects.get(id=id)
    return JsonResponse(
        shoe,
        encoder=ShoeListEncoder,
        safe=False,
    )
