from django.contrib import admin

# Register your models here.

from .models import Shoe
# from wardrobe.api.wardrobe_api.models import Bin

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass

# @admin.register(Bin)
# class BinAdmin(admin.ModelAdmin):
#     pass
