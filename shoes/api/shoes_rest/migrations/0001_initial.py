# Generated by Django 4.0.3 on 2023-12-18 07:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=150, unique=True)),
                ('closet_name', models.CharField(max_length=150)),
                ('bin_number', models.PositiveSmallIntegerField(null=True)),
                ('bin_size', models.PositiveSmallIntegerField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer', models.CharField(max_length=150)),
                ('model_name', models.CharField(max_length=150)),
                ('color', models.CharField(max_length=150)),
                ('picture_url', models.URLField(null=True)),
                ('bin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bins', to='shoes_rest.binvo')),
            ],
        ),
    ]
