from django.contrib import admin

from .models import Hats

@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    list_display = [
        "fabric_type",
        "style_name",
        "color",
        "picture_url"
    ]
